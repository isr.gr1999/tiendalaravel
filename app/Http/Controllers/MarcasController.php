<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Marcas;
use RealRashid\SweetAlert\Facades\Alert;

class MarcasController extends Controller
{

    public function index()
    {
        $marca= Marcas::all();
        return view('marcas',compact('marca'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //Este metodo es el encargasdo de guardar en la BD
        $marca = new Marcas($request ->input());
        $marca->saveOrFail();
        Alert::toast('Marca Añadida','success')->background('#DDE7C7')->position('top-end')->autoClose(3000);
        return redirect('marcas');
    }


    public function show(string $id)
    {
        $marca= Marcas::find($id);
        return view('editMarca',compact('marca'));
    }

    public function edit(string $id)
    {
        //
    }


    public function update(Request $request, string $id)
    {
        $marca= Marcas::find($id);
        $marca->fill($request ->input())->saveOrFail();
        Alert::toast('Marca Actualizada','success')->background('#DDE7C7')->position('top-end')->autoClose(3000);
        return redirect('marcas');
    }

    public function destroy(string $id)
    {
        $marca= Marcas::find($id);
        $marca->delete();
        return redirect('marcas')->with('eliminar','eliminado');
    }
}
