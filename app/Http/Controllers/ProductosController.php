<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productos;
use App\Models\Marcas;
use RealRashid\SweetAlert\Facades\Alert;


class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $productos= Productos::select('productos.id','nombre','genero','id_marca', 'marca')->join('marcas','marcas.id','=','productos.id_marca')->get();
        $marcas= Marcas::all();
        return view('productos',compact('productos','marcas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $productos = new Productos($request ->input());
        $productos->saveOrFail();
        Alert::toast('Producto añadido','success')->background('#DDE7C7')->position('top-end')->autoClose(3000);
        return redirect('productos');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $producto = Productos::find($id);
        $marcas= Marcas::all();
        return view('editProductos',compact('producto','marcas'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,$id)
    {
        $producto= Productos::find($id);
        $producto->fill($request ->input())->saveOrFail(); 
        Alert::toast('Producto Actualizado','success')->background('#DDE7C7')->position('top-end')->autoClose(3000);
        return redirect('productos');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $producto= Productos::find($id);
        $producto->delete();
        return redirect('productos');
    }
}
