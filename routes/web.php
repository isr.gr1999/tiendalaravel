<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('plantilla');
});
Route::resource('marcas', App\Http\Controllers\MarcasController::class);
Route::resource('productos', App\Http\Controllers\ProductosController::class);
