@extends('plantilla')
@section('contenido')
    <div class="row mt-3">
        <div class="col-md-4 offset-md-4">
            <div class="d-grid mx-auto">
                <button class="btn btn-dark openModal" style="background-color: #C6E0FF;" data-bs-toggle="modal" data-bs-target="#modalProductos">
                    <i class="fa-solid fa-circle-plus"></i> Añadir Producto
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 col-lg-8 offset-0 offset-lg-2 offset-md-4">
            <div class="table responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>#</th>
                        <th>NOMBRE</th>
                        <th>GENERO</th>
                        <th>MARCA</th>
                        <th class="text-center">EDITAR</th>
                        <th class="text-center">ELIMINAR</th>
                    </thead>
                    <tbody class="table-group-divider">
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($productos as $row)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $row->nombre }}</td>
                                <td>{{ $row->genero }}</td>
                                <td>{{ $row->marca }}</td>
                                <td class="text-center">
                                    <a href="{{ url('productos', [$row]) }}" class="btn btn-warning">
                                        <i class="fa-solid fa-edit"></i> </a>
                                </td>
                                <td class="text-center">
                                    <form action="{{ url('productos', [$row]) }}" method="POST">
                                        @method('delete')
                                        @csrf
                                        <button class="btn btn-danger">
                                            <i class="fa-solid fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalProductos" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="h5" id="titulo_modal">Añadir Producto</label>
                    <button type="button" class="btn btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="frmMarcas" method="POST" action="{{ url('productos') }}">
                        @csrf
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-shop"></i></i></span>
                            <input type="text" name="nombre" class="form-control" maxlength="50" placeholder="Nombre"
                                required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-person-half-dress"></i></span>
                            <input type="text" name="genero" class="form-control" maxlength="50" placeholder="Genero"
                                required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-user"></i></span>
                            <select name="id_marca" class="form-select" required>
                                <option value="">Marca</option>
                                @foreach ($marcas as $row)
                                    <option value="{{$row->id}}">{{$row->marca}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-grid col-6 mx-auto">
                            <button type="submit" class="btn btn-success" id="btnGuardar">
                                <i class="fa-solid fa-floppy-disk"></i> Guardar
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCerrar" class="btn btn-danger" data-bs-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
