@extends('plantilla')
@section('contenido')
    <div class="row mt-3">
        <div class="col-md-4 offset-md-4">
            <div class="card">
                <div class="card-header text-white" style="background-color: #26408B;">Editar Producto</div>
                <div class="card-body">
                    <form id="frmMarcas" method="POST" action="{{ url('productos',[$producto]) }}">
                        @csrf
                        @method('PUT')
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-shop"></i></span>
                            <input type="text" name="nombre" value="{{ $producto->nombre }}" class="form-control" maxlength="50" placeholder="Nombre"
                                required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-person-half-dress"></i></span>
                            <input type="text" name="genero" value="{{ $producto->genero }}" class="form-control" maxlength="50" placeholder="Genero"
                                required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-user"></i></span>
                            <select name="id_marca" class="form-select" required>
                                <option value="">Marca</option>
                                @foreach ($marcas as $row)
                                    @if ($row->id == $producto->id_marca)
                                    <option selected value="{{ $row->id }}">{{ $row->marca }}</option>
                                    @else
                                    <option value="{{ $row->id }}">{{ $row->marca }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="d-grid col-6 mx-auto">
                            <button type="submit" class="btn btn-success" id="btnGuardar">
                                <i class="fa-solid fa-floppy-disk"></i> Guardar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
