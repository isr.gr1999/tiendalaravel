@extends('plantilla')
@section('contenido')
    <div class="row mt-3">
        <div class="col-md-4 offset-md-4">
            <div class="card">
                <div class="card-header text-white" style="background-color: #26408B;">Editar Marca</div>
                <div class="card-body">
                    <form id="frmMarcas" method="POST"  action="{{url('marcas',[$marca])}}">
                        @method('PUT')
                        @csrf
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-user"></i></span>
                            <input type="text" name="marca" value="{{ $marca->marca }}" class="form-control" maxlength="50" placeholder="Marca" required>
                        </div>
                        <div class="d-grid col-6 mx-auto">
                            <button type="submit" class="btn btn-success" id="btnGuardar" >
                                <i class="fa-solid fa-user"></i> Guardar
                            </button>                        
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection