@extends('plantilla')
@section('contenido')
    <div class="row mt-3">
        <div class="col-md-4 offset-md-4">
            <div class="d-grid mx-auto">
                <button class="btn btn-dark openModal border-none" style="background-color: #92B4F4" data-bs-toggle="modal" data-bs-target="#modalMarcas">
                    <i class="fa-solid fa-circle-plus"></i> Añadir Marca
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 col-lg-8 offset-0 offset-lg-2 offset-md-4">
            <div class="table responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>#</th>
                        <th>MARCA</th>
                        <th class="text-center">EDITAR</th>
                        <th class="text-center">ELIMINAR</th>
                    </thead>
                    <tbody class="table-group-divider">
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($marca as $row)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $row->marca }}</td>
                                <td class="text-center">
                                    <a href="{{ url('marcas', [$row]) }}" class="btn btn-warning">
                                        <i class="fa-solid fa-edit"></i> </a>
                                </td>
                                <td class="text-center">
                                    <form action="{{ url('marcas', [$row]) }}" method="POST">
                                        @method('delete')
                                        @csrf
                                        <button class="btn btn-danger btn-delete">
                                            <i class="fa-solid fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalMarcas" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="h5" id="titulo_modal">Añadir Marca</label>
                    <button type="button"  class="btn btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="frmMarcas" method="POST" action="{{ url('marcas') }}">
                        @csrf
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-building"></i></span>
                            <input type="text" name="marca" class="form-control" maxlength="50" placeholder="Marca"
                                required>
                        </div>
                        <div class="d-grid col-6 mx-auto">
                            <button type="submit" id="frmMarca" class="btn btn-success" style="background-color: #AFD0BF;">
                                <i class="fa-solid fa-floppy-disk"></i></i> Guardar
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnCerrar" class="btn btn-danger" data-bs-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('alert')


@endsection
