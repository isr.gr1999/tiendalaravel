<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRUD TiendaLaravel</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #26408B;">
        <div class="container-fluid">
            <a class="navbar-brand text-white" href="/">Tienda de Ropa</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active text-white" aria-current="page" href="/marcas">Marcas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="/productos">Productos</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    @if(Request::is('/'))
    <div class="container-fluid">
        <div class="col-md-6 mt-5 offset-md-3 text-center" style="border-radius: 50px; background-color:#AFD0BF;">
            <img src="{{ asset('asset/saludo.gif') }}" alt="">
        </div>
    </div>
    @endif
    <div class="container-fluid">
        @yield('contenido')    
    </div>
    @include('sweetalert::alert')
    @yield('alert')
</body>
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<script src="https://kit.fontawesome.com/9560d249ee.js" crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}"></script>
</html>
